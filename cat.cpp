///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "cat.hpp"

using namespace std;

namespace animalfarm {
	
Cat::Cat( string newName, enum Color newColor, enum Gender newGender ) {
	gender = newGender;
	species = "Felis catus";
	hairColor = newColor;
	gestationPeriod = 60;
	name = newName;
}


 const string Cat::speak() {
	return string( "Meow" );
}


void Cat::printInfo() {
	cout << "Cat Name = [" << name << "]" << endl;
	Mammal::printInfo();
}

} // namespace animalfarm
